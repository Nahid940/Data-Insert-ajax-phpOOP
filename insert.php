<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
<!--    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>-->
<!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">

</head>
<body>
<div class="form">

    <ul class="tab-group">
        <li class="tab active"><a href="#signup">Sign Up</a></li>
        <li class="tab"><a href="#login">Insert data</a></li>
    </ul>

    <div class="tab-content">
<!--        <div id="signup">-->
<!--            <h1>Sign Up for Free</h1>-->
<!---->
<!--            <form action="/" method="post">-->
<!---->
<!--                <div class="top-row">-->
<!--                    <div class="field-wrap">-->
<!--                        <label>-->
<!--                            First Name<span class="req">*</span>-->
<!--                        </label>-->
<!--                        <input type="text" required autocomplete="off" />-->
<!--                    </div>-->
<!---->
<!--                    <div class="field-wrap">-->
<!--                        <label>-->
<!--                            Last Name<span class="req">*</span>-->
<!--                        </label>-->
<!--                        <input type="text"required autocomplete="off"/>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="field-wrap">-->
<!--                    <label>-->
<!--                        Email Address<span class="req">*</span>-->
<!--                    </label>-->
<!--                    <input type="email"required autocomplete="off"/>-->
<!--                </div>-->
<!---->
<!--                <div class="field-wrap">-->
<!--                    <label>-->
<!--                        Set A Password<span class="req">*</span>-->
<!--                    </label>-->
<!--                    <input type="password"required autocomplete="off"/>-->
<!--                </div>-->
<!---->
<!--                <button type="submit" class="button button-block"/>Get Started</button>-->
<!---->
<!--            </form>-->
<!---->
<!--        </div>-->

        <div id="login">
            <center><h4><span class="label label-success" id="done"></span></h4></center>
            <center><h4><span class="label label-danger" id="result"></span></h4></center>
            <h1>Welcome !</h1>

            <form action="/" method="post" id="isertData">

                <div class="field-wrap">
                    <span class="label label-primary">First name</span>
                    <input type="text" required autocomplete="off" name="fname" id="fname" class="form-control"/>

                </div>

                <div class="field-wrap">
                    <span class="label label-primary">Last name</span>
                    <input type="text"required autocomplete="off" name="lname" id="lname" class="form-control"/>
                </div>

<!--                <p class="forgot"><a href="#">Forgot Password?</a></p>-->

                <button class="button button-block" name="button" id="button"/>Insert data</button>

            </form>

        </div>

    </div><!-- tab-content -->

</div>



<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src="js/index.js"></script>

    <script>
        $(document).ready(function(){
            $('#isertData').submit(function(e){
                e.preventDefault();
                var fname = $('#fname').val();
                var lname = $('#lname').val();
                if(fname != '' || lname !=''){
                    $.ajax({
                        type: "POST",
                        data:{
                            fname: fname,
                            lname: lname
                        },
                        url: "insertuser.php",

                        success: function(responseText){
                            if(responseText==1){
                                $("#done").html("Successfully inserted !");
                            }else {
                                $("#result").html(responseText);

                            }
                        }

                    });
                }else{
                    $("#result").text("Fill up each field !!");
                    setTimeout(function(){
                        $('#result').fadeOut();
//                            location.reload();
                        window.location = document.URL;
                    },1000);
                }

                return false;
            });
        });
    </script>
</body>
</html>





